package com.adsi.restaurant.repository;

import com.adsi.restaurant.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, Integer> {
    Optional<Board> findByNumberAndAndRestaurant_Id(Integer number, Integer restaurantId);
}
