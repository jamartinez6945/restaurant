package com.adsi.restaurant.service;

import com.adsi.restaurant.service.dto.BoardDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BoardService {

    BoardDto create(BoardDto boardDto);
    Page<BoardDto> findAll(Pageable pageable);
    BoardDto findById(Integer id);
    
}
