package com.adsi.restaurant.service.transformer;

import com.adsi.restaurant.domain.Restaurant;
import com.adsi.restaurant.service.dto.RestaurantDto;


public class RestaurantTransformer {

    public static Restaurant getRestaurantByRestaurantDto(RestaurantDto dto){
        if(dto == null){
            return null;
        }
        Restaurant restaurant = new Restaurant();
        restaurant.setAddress(dto.getAddress());
        restaurant.setDescription(dto.getDescription());
        restaurant.setId(dto.getId());
        restaurant.setName(dto.getName());
        return restaurant;
    }

    public static RestaurantDto getRestaurantDtoByRestaurant(Restaurant restaurant){
        if(restaurant == null){
            return null;
        }
        RestaurantDto dto = new RestaurantDto();
        dto.setAddress(restaurant.getAddress());
        dto.setDescription(restaurant.getDescription());
        dto.setId(restaurant.getId());
        dto.setName(restaurant.getName());
        return dto;
    }
}
