package com.adsi.restaurant.service.imp;

import com.adsi.restaurant.domain.Board;
import com.adsi.restaurant.domain.Restaurant;
import com.adsi.restaurant.repository.BoardRepository;
import com.adsi.restaurant.repository.RestaurantRepository;
import com.adsi.restaurant.service.BoardService;
import com.adsi.restaurant.service.dto.BoardDto;
import com.adsi.restaurant.service.exception.ObjectError;
import com.adsi.restaurant.service.exception.ObjectException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoardServiceImp implements BoardService {

    @Autowired
    BoardRepository repository;

    @Autowired
    RestaurantRepository restaurantRepository;

    public static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public BoardDto create(BoardDto boardDto) {
        Optional<Restaurant> restaurant = restaurantRepository.findById(boardDto.getRestaurant().getId());
        if (boardDto.getNumber() <= 0) {
            throw new ObjectException("El número no es valida",
                    new ObjectError(HttpStatus.BAD_REQUEST, "El número de mesa no puede ser menor que cero"));

        }
        if (boardDto.getCapacity() <= 0) {
            throw new ObjectException("La capacidad no es valida",
                    new ObjectError(HttpStatus.BAD_REQUEST, "La capacidad de la mesa no puede ser menor que cero"));

        }
        if (!restaurant.isPresent()){
            throw new ObjectException("El restaurante no es valido",
                    new ObjectError(HttpStatus.NOT_FOUND, "El restaurante con id:"+ boardDto.getRestaurant().getId() + " no existe"));
        }
        Optional<Board> optionalBoard = repository.findByNumberAndAndRestaurant_Id(boardDto.getNumber(), restaurant.get().getId());
        if(optionalBoard.isPresent()){
            throw new ObjectException("El numero de mesa ya existe",
                    new ObjectError(HttpStatus.BAD_REQUEST, "La mesa número "+ boardDto.getNumber() + " ya existe"));
        }
        return modelMapper.map(repository.save(modelMapper.map(boardDto, Board.class)), BoardDto.class);
    }

    @Override
    public Page<BoardDto> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(board -> modelMapper.map(board, BoardDto.class));
    }

    @Override
    public BoardDto findById(Integer id) {
        Optional<Board> optionalBoard = repository.findById(id);
        if (!optionalBoard.isPresent()) {
            throw new ObjectException("El número de mesa no fue encontrado",
                    new ObjectError(HttpStatus.NOT_FOUND, "La mesa con id:" + id + " no fue encontrada"));
        }
        return modelMapper.map(optionalBoard.get(), BoardDto.class);
    }
}
