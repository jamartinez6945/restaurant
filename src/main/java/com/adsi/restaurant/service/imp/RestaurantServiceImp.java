package com.adsi.restaurant.service.imp;

import com.adsi.restaurant.domain.Restaurant;
import com.adsi.restaurant.repository.RestaurantRepository;
import com.adsi.restaurant.service.RestaurantService;
import com.adsi.restaurant.service.dto.RestaurantDto;
import com.adsi.restaurant.service.transformer.RestaurantTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RestaurantServiceImp implements RestaurantService {

    @Autowired
    RestaurantRepository repository;

    @Override
    public List<RestaurantDto> findAll() {
        return repository.findAll()
                .stream()
                .map(RestaurantTransformer::getRestaurantDtoByRestaurant)
                .collect(Collectors.toList());
    }

    @Override
    public RestaurantDto findById(Integer id) {
        Optional<Restaurant> optional = repository.findById(id);
        if(optional.isPresent()){
            return RestaurantTransformer.getRestaurantDtoByRestaurant(optional.get());
        }
        return null;
    }
}
