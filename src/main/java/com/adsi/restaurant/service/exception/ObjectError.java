package com.adsi.restaurant.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class ObjectError {

    private final HttpStatus status;
    private final String description;
}
