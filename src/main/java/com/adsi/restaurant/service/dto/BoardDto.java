package com.adsi.restaurant.service.dto;

import com.adsi.restaurant.domain.Restaurant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class BoardDto implements Serializable {
    @Id
    private int id;

    private int capacity;
    private int number;
    private Restaurant restaurant;
}
