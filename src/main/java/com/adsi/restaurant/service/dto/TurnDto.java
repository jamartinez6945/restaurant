package com.adsi.restaurant.service.dto;

import com.adsi.restaurant.domain.Restaurant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class TurnDto implements Serializable {

    @Id
    private int id;
    @NotEmpty(message = "Este campo no puede estar vacio")
    private String name;
    private Restaurant restaurant;
}
