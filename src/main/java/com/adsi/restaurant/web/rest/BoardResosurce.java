package com.adsi.restaurant.web.rest;

import com.adsi.restaurant.service.BoardService;
import com.adsi.restaurant.service.dto.BoardDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/board")
public class BoardResosurce {

    @Autowired
    BoardService service;

    @PostMapping("")
    public ResponseEntity<BoardDto> create(@RequestBody BoardDto boardDto) {
        return new ResponseEntity<>(service.create(boardDto), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BoardDto> findById(@PathVariable Integer id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Page<BoardDto>> findAll(@PageableDefault(page = 0, size = 10) Pageable pageable){
        return new ResponseEntity<>(service.findAll(pageable), HttpStatus.OK);
    }

}
